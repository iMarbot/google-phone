.class public final Lcaq;
.super Lcad;
.source "PG"


# instance fields
.field public final b:Lcao;

.field public final c:Z

.field private d:[Lcbf;

.field private e:[Lcan;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1
    invoke-direct {p0}, Lcad;-><init>()V

    .line 2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 3
    iget v1, v0, Landroid/util/DisplayMetrics;->xdpi:F

    iget v2, v0, Landroid/util/DisplayMetrics;->ydpi:F

    add-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    .line 4
    new-instance v2, Lcae;

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v0, v0

    invoke-direct {v2, v1, v0}, Lcae;-><init>(FF)V

    iput-object v2, p0, Lcaq;->a:Lcae;

    .line 5
    new-instance v0, Lcao;

    invoke-direct {v0}, Lcao;-><init>()V

    iput-object v0, p0, Lcaq;->b:Lcao;

    .line 7
    invoke-static {p1}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "answer_false_touch_detection_enabled"

    .line 8
    invoke-interface {v0, v1, v4}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcaq;->c:Z

    .line 9
    const/16 v0, 0x9

    new-array v0, v0, [Lcbf;

    new-instance v1, Lcab;

    iget-object v2, p0, Lcaq;->a:Lcae;

    invoke-direct {v1, v2}, Lcab;-><init>(Lcae;)V

    aput-object v1, v0, v5

    new-instance v1, Lcba;

    invoke-direct {v1}, Lcba;-><init>()V

    aput-object v1, v0, v4

    new-instance v1, Lcag;

    invoke-direct {v1}, Lcag;-><init>()V

    aput-object v1, v0, v6

    const/4 v1, 0x3

    new-instance v2, Lcak;

    iget-object v3, p0, Lcaq;->a:Lcae;

    invoke-direct {v2, v3}, Lcak;-><init>(Lcae;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcai;

    iget-object v3, p0, Lcaq;->a:Lcae;

    invoke-direct {v2, v3}, Lcai;-><init>(Lcae;)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lbzz;

    iget-object v3, p0, Lcaq;->a:Lcae;

    invoke-direct {v2, v3}, Lbzz;-><init>(Lcae;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcax;

    iget-object v3, p0, Lcaq;->a:Lcae;

    invoke-direct {v2, v3}, Lcax;-><init>(Lcae;)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lcar;

    invoke-direct {v2}, Lcar;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Lcaf;

    invoke-direct {v2}, Lcaf;-><init>()V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcaq;->d:[Lcbf;

    .line 10
    new-array v0, v6, [Lcan;

    new-instance v1, Lcau;

    invoke-direct {v1}, Lcau;-><init>()V

    aput-object v1, v0, v5

    new-instance v1, Lcav;

    invoke-direct {v1}, Lcav;-><init>()V

    aput-object v1, v0, v4

    iput-object v0, p0, Lcaq;->e:[Lcan;

    .line 11
    return-void
.end method


# virtual methods
.method public final a(Landroid/hardware/SensorEvent;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 79
    iget-object v2, p0, Lcaq;->d:[Lcbf;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 80
    invoke-virtual {v4, p1}, Lcad;->a(Landroid/hardware/SensorEvent;)V

    .line 81
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 82
    :cond_0
    iget-object v1, p0, Lcaq;->e:[Lcan;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 83
    invoke-virtual {v3, p1}, Lcad;->a(Landroid/hardware/SensorEvent;)V

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 85
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 12

    .prologue
    .line 12
    .line 13
    iget-object v3, p0, Lcaq;->a:Lcae;

    .line 14
    iget-object v0, v3, Lcae;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 15
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v4

    .line 16
    if-nez v4, :cond_0

    .line 17
    iget-object v0, v3, Lcae;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 18
    :cond_0
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 19
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    .line 20
    iget-object v0, v3, Lcae;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 21
    iget-object v0, v3, Lcae;->a:Landroid/util/SparseArray;

    new-instance v1, Lcbe;

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 22
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v6

    iget v8, v3, Lcae;->c:F

    invoke-direct {v1, v6, v7, v8}, Lcbe;-><init>(JF)V

    .line 23
    invoke-virtual {v0, v5, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 24
    :cond_1
    iget-object v0, v3, Lcae;->a:Landroid/util/SparseArray;

    .line 25
    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbe;

    .line 26
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v8

    .line 28
    iput-wide v8, v0, Lcbe;->c:J

    .line 29
    new-instance v7, Lcat;

    iget v10, v0, Lcbe;->e:F

    div-float/2addr v1, v10

    iget v10, v0, Lcbe;->e:F

    div-float/2addr v6, v10

    iget-wide v10, v0, Lcbe;->b:J

    sub-long/2addr v8, v10

    invoke-direct {v7, v1, v6, v8, v9}, Lcat;-><init>(FFJ)V

    .line 30
    iget-object v1, v0, Lcbe;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 31
    iget v6, v0, Lcbe;->d:F

    iget-object v1, v0, Lcbe;->a:Ljava/util/ArrayList;

    iget-object v8, v0, Lcbe;->a:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcat;

    invoke-virtual {v1, v7}, Lcat;->a(Lcat;)F

    move-result v1

    add-float/2addr v1, v6

    iput v1, v0, Lcbe;->d:F

    .line 32
    :cond_2
    iget-object v0, v0, Lcbe;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    const/4 v0, 0x1

    if-eq v4, v0, :cond_3

    const/4 v0, 0x3

    if-eq v4, v0, :cond_3

    const/4 v0, 0x6

    if-ne v4, v0, :cond_4

    .line 34
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    if-ne v2, v0, :cond_4

    .line 35
    :cond_3
    iget-object v0, v3, Lcae;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Lcae;->a(I)Lcbe;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 37
    :cond_5
    iget-object v1, p0, Lcaq;->d:[Lcbf;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 38
    invoke-virtual {v3, p1}, Lcbf;->a(Landroid/view/MotionEvent;)V

    .line 39
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 40
    :cond_6
    iget-object v1, p0, Lcaq;->e:[Lcan;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 41
    invoke-virtual {v3, p1}, Lcan;->a(Landroid/view/MotionEvent;)V

    .line 42
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 43
    :cond_7
    iget-object v0, p0, Lcaq;->a:Lcae;

    .line 44
    iget-object v0, v0, Lcae;->b:Ljava/util/ArrayList;

    .line 45
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 46
    const/4 v0, 0x0

    move v3, v0

    :goto_3
    if-ge v3, v4, :cond_9

    .line 47
    iget-object v0, p0, Lcaq;->a:Lcae;

    .line 48
    iget-object v0, v0, Lcae;->b:Ljava/util/ArrayList;

    .line 49
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbe;

    .line 50
    const/4 v2, 0x0

    .line 51
    iget-object v5, p0, Lcaq;->d:[Lcbf;

    array-length v6, v5

    const/4 v1, 0x0

    :goto_4
    if-ge v1, v6, :cond_8

    aget-object v7, v5, v1

    .line 52
    invoke-virtual {v7, v0}, Lcbf;->a(Lcbe;)F

    move-result v7

    .line 53
    add-float/2addr v2, v7

    .line 54
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 55
    :cond_8
    iget-object v0, p0, Lcaq;->b:Lcao;

    .line 56
    invoke-virtual {v0}, Lcao;->a()V

    .line 57
    iget-object v0, v0, Lcao;->a:Ljava/util/ArrayList;

    new-instance v1, Lcap;

    invoke-direct {v1, v2}, Lcap;-><init>(F)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 59
    :cond_9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 60
    const/4 v1, 0x1

    if-eq v0, v1, :cond_a

    const/4 v1, 0x3

    if-ne v0, v1, :cond_c

    .line 61
    :cond_a
    const/4 v1, 0x0

    .line 62
    iget-object v2, p0, Lcaq;->e:[Lcan;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_5
    if-ge v0, v3, :cond_b

    aget-object v4, v2, v0

    .line 63
    invoke-virtual {v4}, Lcan;->a()F

    move-result v4

    .line 64
    add-float/2addr v1, v4

    .line 65
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 66
    :cond_b
    iget-object v0, p0, Lcaq;->b:Lcao;

    .line 67
    invoke-virtual {v0}, Lcao;->a()V

    .line 68
    iget-object v0, v0, Lcao;->b:Ljava/util/ArrayList;

    new-instance v2, Lcap;

    invoke-direct {v2, v1}, Lcap;-><init>(F)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    :cond_c
    iget-object v1, p0, Lcaq;->a:Lcae;

    .line 70
    iget-object v0, v1, Lcae;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 71
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    .line 72
    const/4 v0, 0x0

    :goto_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    if-ge v0, v3, :cond_f

    .line 73
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    .line 74
    const/4 v4, 0x1

    if-eq v2, v4, :cond_d

    const/4 v4, 0x3

    if-eq v2, v4, :cond_d

    const/4 v4, 0x6

    if-ne v2, v4, :cond_e

    .line 75
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v4

    if-ne v0, v4, :cond_e

    .line 76
    :cond_d
    iget-object v4, v1, Lcae;->a:Landroid/util/SparseArray;

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->remove(I)V

    .line 77
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 78
    :cond_f
    return-void
.end method
