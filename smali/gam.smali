.class final Lgam;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/ArrayList;

.field public b:J


# direct methods
.method constructor <init>(Ljava/lang/String;J)V
    .locals 10

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgam;->a:Ljava/util/ArrayList;

    .line 3
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lgam;->b:J

    .line 4
    iget-wide v2, p0, Lgam;->b:J

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v6, p2

    move-wide v8, p2

    invoke-virtual/range {v0 .. v9}, Lgam;->a(Ljava/lang/String;JJJJ)J

    .line 5
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;JJ)J
    .locals 10

    .prologue
    const-wide/16 v4, 0x1

    .line 6
    iget-wide v0, p0, Lgam;->b:J

    add-long v2, v0, v4

    iput-wide v2, p0, Lgam;->b:J

    move-object v0, p0

    move-object v1, p1

    move-wide v6, p2

    move-wide v8, p4

    invoke-virtual/range {v0 .. v9}, Lgam;->a(Ljava/lang/String;JJJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method final a(Ljava/lang/String;JJJJ)J
    .locals 4

    .prologue
    .line 7
    cmp-long v0, p8, p6

    if-gez v0, :cond_1

    .line 8
    const-string v1, "PrimesStartupTracer"

    const-string v2, "endTime < startTime. Dropping span: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lfmk;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9
    const-wide/16 p2, -0x1

    .line 17
    :goto_1
    return-wide p2

    .line 8
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 10
    :cond_1
    new-instance v0, Lhsy;

    invoke-direct {v0}, Lhsy;-><init>()V

    .line 11
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhsy;->b:Ljava/lang/Long;

    .line 12
    iput-object p1, v0, Lhsy;->a:Ljava/lang/String;

    .line 13
    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhsy;->d:Ljava/lang/Long;

    .line 14
    sub-long v2, p8, p6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhsy;->e:Ljava/lang/Long;

    .line 15
    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhsy;->c:Ljava/lang/Long;

    .line 16
    iget-object v1, p0, Lgam;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
