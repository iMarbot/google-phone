.class final Lbjm;
.super Lbjn;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:J

.field private c:J


# direct methods
.method constructor <init>(Ljava/lang/String;JJ)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Lbjn;-><init>()V

    .line 2
    if-nez p1, :cond_0

    .line 3
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null number"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4
    :cond_0
    iput-object p1, p0, Lbjm;->a:Ljava/lang/String;

    .line 5
    iput-wide p2, p0, Lbjm;->b:J

    .line 6
    iput-wide p4, p0, Lbjm;->c:J

    .line 7
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lbjm;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 9
    iget-wide v0, p0, Lbjm;->b:J

    return-wide v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 10
    iget-wide v0, p0, Lbjm;->c:J

    return-wide v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 11
    if-ne p1, p0, :cond_1

    .line 19
    :cond_0
    :goto_0
    return v0

    .line 13
    :cond_1
    instance-of v2, p1, Lbjn;

    if-eqz v2, :cond_3

    .line 14
    check-cast p1, Lbjn;

    .line 15
    iget-object v2, p0, Lbjm;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lbjn;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lbjm;->b:J

    .line 16
    invoke-virtual {p1}, Lbjn;->b()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Lbjm;->c:J

    .line 17
    invoke-virtual {p1}, Lbjn;->c()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    .line 18
    goto :goto_0

    :cond_3
    move v0, v1

    .line 19
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 8

    .prologue
    const/16 v7, 0x20

    const v6, 0xf4243

    .line 20
    iget-object v0, p0, Lbjm;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    xor-int/2addr v0, v6

    .line 21
    mul-int/2addr v0, v6

    .line 22
    iget-wide v2, p0, Lbjm;->b:J

    ushr-long/2addr v2, v7

    iget-wide v4, p0, Lbjm;->b:J

    xor-long/2addr v2, v4

    long-to-int v1, v2

    xor-int/2addr v0, v1

    .line 23
    mul-int/2addr v0, v6

    .line 24
    iget-wide v2, p0, Lbjm;->c:J

    ushr-long/2addr v2, v7

    iget-wide v4, p0, Lbjm;->c:J

    xor-long/2addr v2, v4

    long-to-int v1, v2

    xor-int/2addr v0, v1

    .line 25
    return v0
.end method
