.class public final Lbdc;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/lang/CharSequence;

.field public final b:Ljava/lang/CharSequence;

.field public final c:Ljava/lang/CharSequence;

.field public final d:Ljava/lang/CharSequence;

.field public final e:Ljava/lang/CharSequence;

.field public final f:Ljava/lang/CharSequence;

.field public final g:Ljava/lang/CharSequence;

.field public final h:Ljava/lang/CharSequence;

.field public final i:Ljava/lang/CharSequence;

.field public final j:Ljava/lang/CharSequence;

.field public final k:Ljava/lang/CharSequence;

.field public final l:Ljava/lang/CharSequence;

.field public final m:Ljava/lang/CharSequence;

.field public final n:Ljava/lang/CharSequence;

.field public final o:Ljava/lang/CharSequence;

.field public final p:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lbis;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const v0, 0x7f110300

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdc;->a:Ljava/lang/CharSequence;

    .line 3
    const v0, 0x7f110302

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdc;->b:Ljava/lang/CharSequence;

    .line 4
    const v0, 0x7f110307

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdc;->c:Ljava/lang/CharSequence;

    .line 5
    const v0, 0x7f110309

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdc;->d:Ljava/lang/CharSequence;

    .line 6
    const v0, 0x7f110305

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdc;->e:Ljava/lang/CharSequence;

    .line 7
    const v0, 0x7f110303

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdc;->f:Ljava/lang/CharSequence;

    .line 8
    const v0, 0x7f110304

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdc;->g:Ljava/lang/CharSequence;

    .line 9
    const v0, 0x7f11030a

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdc;->h:Ljava/lang/CharSequence;

    .line 10
    const v0, 0x7f11030b

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdc;->i:Ljava/lang/CharSequence;

    .line 11
    const v0, 0x7f110306

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdc;->j:Ljava/lang/CharSequence;

    .line 12
    const v0, 0x7f11030d

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdc;->k:Ljava/lang/CharSequence;

    .line 13
    const v0, 0x7f11030c

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdc;->l:Ljava/lang/CharSequence;

    .line 14
    const v0, 0x7f1102ff

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdc;->m:Ljava/lang/CharSequence;

    .line 15
    const v0, 0x7f1102fe

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdc;->n:Ljava/lang/CharSequence;

    .line 16
    invoke-interface {p2}, Lbis;->c()I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 17
    invoke-interface {p2}, Lbis;->c()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdc;->o:Ljava/lang/CharSequence;

    .line 19
    :goto_0
    invoke-interface {p2}, Lbis;->b()I

    move-result v0

    if-eq v0, v1, :cond_1

    .line 20
    invoke-interface {p2}, Lbis;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdc;->p:Ljava/lang/CharSequence;

    .line 22
    :goto_1
    return-void

    .line 18
    :cond_0
    iget-object v0, p0, Lbdc;->f:Ljava/lang/CharSequence;

    iput-object v0, p0, Lbdc;->o:Ljava/lang/CharSequence;

    goto :goto_0

    .line 21
    :cond_1
    iget-object v0, p0, Lbdc;->h:Ljava/lang/CharSequence;

    iput-object v0, p0, Lbdc;->p:Ljava/lang/CharSequence;

    goto :goto_1
.end method
