.class public final Lddi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcwv;
.implements Lcwz;


# instance fields
.field private a:Landroid/graphics/Bitmap;

.field private b:Landroid/content/res/Resources;

.field private c:Lcxl;


# direct methods
.method private constructor <init>(Landroid/content/res/Resources;Lcxl;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-static {p1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, Lddi;->b:Landroid/content/res/Resources;

    .line 4
    invoke-static {p2}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxl;

    iput-object v0, p0, Lddi;->c:Lcxl;

    .line 5
    invoke-static {p3}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lddi;->a:Landroid/graphics/Bitmap;

    .line 6
    return-void
.end method

.method public static a(Landroid/content/res/Resources;Lcxl;Landroid/graphics/Bitmap;)Lddi;
    .locals 1

    .prologue
    .line 1
    new-instance v0, Lddi;

    invoke-direct {v0, p0, p1, p2}, Lddi;-><init>(Landroid/content/res/Resources;Lcxl;Landroid/graphics/Bitmap;)V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 7
    const-class v0, Landroid/graphics/drawable/BitmapDrawable;

    return-object v0
.end method

.method public final synthetic b()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 13
    .line 14
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lddi;->b:Landroid/content/res/Resources;

    iget-object v2, p0, Lddi;->a:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 15
    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lddi;->a:Landroid/graphics/Bitmap;

    invoke-static {v0}, Ldhw;->a(Landroid/graphics/Bitmap;)I

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 9
    iget-object v0, p0, Lddi;->c:Lcxl;

    iget-object v1, p0, Lddi;->a:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1}, Lcxl;->a(Landroid/graphics/Bitmap;)V

    .line 10
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lddi;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->prepareToDraw()V

    .line 12
    return-void
.end method
