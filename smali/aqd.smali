.class public final Laqd;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lbsu;

.field public final b:Laqf;

.field public final c:Ljava/util/concurrent/BlockingQueue;

.field public final d:Landroid/os/Handler;

.field public e:Lblq;

.field public f:Laqg;

.field public volatile g:Z

.field private h:Lbmm;


# direct methods
.method public constructor <init>(Lbsu;Lbmm;Laqf;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput-boolean v0, p0, Laqd;->g:Z

    .line 3
    iput-object p1, p0, Laqd;->a:Lbsu;

    .line 4
    iput-object p2, p0, Laqd;->h:Lbmm;

    .line 5
    iput-object p3, p0, Laqd;->b:Laqf;

    .line 6
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Laqd;->c:Ljava/util/concurrent/BlockingQueue;

    .line 7
    new-instance v0, Laqe;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v0, v1}, Laqe;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v0, p0, Laqd;->d:Landroid/os/Handler;

    .line 8
    return-void
.end method


# virtual methods
.method final declared-synchronized a()V
    .locals 2

    .prologue
    .line 143
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Laqd;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 150
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 145
    :cond_1
    :try_start_1
    iget-object v0, p0, Laqd;->f:Laqg;

    if-nez v0, :cond_0

    .line 147
    new-instance v0, Laqg;

    invoke-direct {v0, p0}, Laqg;-><init>(Laqd;)V

    iput-object v0, p0, Laqd;->f:Laqg;

    .line 148
    iget-object v0, p0, Laqd;->f:Laqg;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Laqg;->setPriority(I)V

    .line 149
    iget-object v0, p0, Laqd;->f:Laqg;

    invoke-virtual {v0}, Laqg;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lbml;ZI)V
    .locals 2

    .prologue
    .line 158
    new-instance v0, Laqh;

    invoke-direct {v0, p1, p2, p3, p5}, Laqh;-><init>(Ljava/lang/String;Ljava/lang/String;Lbml;I)V

    .line 159
    iget-object v1, p0, Laqd;->c:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1, v0}, Ljava/util/concurrent/BlockingQueue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 160
    iget-object v1, p0, Laqd;->c:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1, v0}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    .line 161
    :cond_0
    if-eqz p4, :cond_1

    .line 162
    invoke-virtual {p0}, Laqd;->a()V

    .line 163
    :cond_1
    return-void
.end method

.method final a(Laqh;)Z
    .locals 14

    .prologue
    const/4 v5, 0x2

    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 9
    new-array v0, v5, [Ljava/lang/Object;

    iget-object v1, p1, Laqh;->a:Ljava/lang/String;

    .line 10
    invoke-static {v1}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    iget v1, p1, Laqh;->d:I

    .line 11
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    .line 12
    invoke-virtual {p1}, Laqh;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 13
    iget-object v0, p0, Laqd;->h:Lbmm;

    iget-object v1, p1, Laqh;->a:Ljava/lang/String;

    iget-object v2, p1, Laqh;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lbmm;->a(Ljava/lang/String;Ljava/lang/String;)Lbml;

    move-result-object v1

    .line 14
    if-eqz v1, :cond_4

    iget-boolean v0, v1, Lbml;->r:Z

    if-nez v0, :cond_4

    .line 15
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 16
    iget-object v0, p0, Laqd;->h:Lbmm;

    iget-object v7, p0, Laqd;->e:Lblq;

    iget-object v8, p1, Laqh;->a:Ljava/lang/String;

    .line 17
    invoke-static {}, Lbdf;->c()V

    .line 18
    iget-object v9, v0, Lbmm;->b:Landroid/content/Context;

    invoke-static {v9}, Lblq;->a(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 19
    if-eqz v7, :cond_3

    .line 20
    iget-object v0, v0, Lbmm;->b:Landroid/content/Context;

    .line 22
    invoke-static {}, Lbdf;->c()V

    .line 23
    new-array v9, v4, [Ljava/lang/Object;

    .line 24
    invoke-static {v8}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v6

    .line 25
    iget-object v9, v7, Lblq;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v9, v8}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 26
    iget-object v0, v7, Lblq;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v8}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lblr;

    .line 34
    :cond_0
    :goto_0
    if-eqz v0, :cond_3

    .line 35
    iget-object v7, v1, Lbml;->d:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    iget-object v7, v0, Lblr;->a:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 36
    iget-object v7, v0, Lblr;->a:Ljava/lang/String;

    iput-object v7, v1, Lbml;->d:Ljava/lang/String;

    .line 37
    :cond_1
    iget-object v7, v0, Lblr;->b:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 38
    iget-object v7, v0, Lblr;->b:Ljava/lang/String;

    iput-object v7, v1, Lbml;->j:Ljava/lang/String;

    .line 39
    sget-object v7, Lbko$a;->g:Lbko$a;

    iput-object v7, v1, Lbml;->q:Lbko$a;

    .line 40
    :cond_2
    iget-boolean v7, v1, Lbml;->r:Z

    if-nez v7, :cond_3

    iget-object v7, v1, Lbml;->m:Landroid/net/Uri;

    if-nez v7, :cond_3

    iget-object v7, v0, Lblr;->c:Ljava/lang/String;

    if-eqz v7, :cond_3

    .line 41
    iget-object v0, v0, Lblr;->c:Ljava/lang/String;

    invoke-static {v0}, Lbib;->d(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v1, Lbml;->m:Landroid/net/Uri;

    .line 42
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    sub-long v2, v8, v2

    .line 43
    new-array v0, v4, [Ljava/lang/Object;

    .line 44
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v6

    .line 45
    :cond_4
    iget v0, p1, Laqh;->d:I

    if-ne v0, v4, :cond_1c

    .line 46
    invoke-static {v1}, Lbmm;->a(Lbml;)Z

    move-result v0

    if-nez v0, :cond_1c

    .line 47
    iget-object v1, p1, Laqh;->a:Ljava/lang/String;

    iget-object v2, p1, Laqh;->b:Ljava/lang/String;

    iget-object v3, p1, Laqh;->c:Lbml;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Laqd;->a(Ljava/lang/String;Ljava/lang/String;Lbml;ZI)V

    .line 142
    :cond_5
    :goto_1
    return v6

    .line 27
    :cond_6
    sget-object v9, Lblq;->a:Landroid/net/Uri;

    .line 28
    invoke-static {v8}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    new-array v11, v4, [Ljava/lang/String;

    const-string v12, "system"

    aput-object v12, v11, v6

    .line 29
    invoke-static {v0, v9, v10, v11}, Lblq;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lblr;

    move-result-object v0

    .line 30
    if-eqz v0, :cond_0

    .line 31
    iget-object v7, v7, Lblq;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7, v8, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 49
    :cond_7
    iget-object v1, p0, Laqd;->h:Lbmm;

    iget-object v2, p1, Laqh;->a:Ljava/lang/String;

    iget-object v3, p1, Laqh;->b:Ljava/lang/String;

    .line 50
    iget-object v0, v1, Lbmm;->c:Lbmi;

    if-eqz v0, :cond_15

    .line 51
    iget-object v0, v1, Lbmm;->b:Landroid/content/Context;

    invoke-static {v0}, Lbmm;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 52
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_8
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 53
    invoke-virtual {v1, v2, v3, v8, v9}, Lbmm;->a(Ljava/lang/String;Ljava/lang/String;J)Lbml;

    move-result-object v0

    .line 54
    invoke-static {v0}, Lbmm;->a(Lbml;)Z

    move-result v7

    if-eqz v7, :cond_8

    :goto_2
    move-object v2, v0

    .line 59
    :goto_3
    if-eqz v2, :cond_5

    .line 61
    new-instance v3, Laqk;

    iget-object v0, p1, Laqh;->a:Ljava/lang/String;

    iget-object v1, p1, Laqh;->b:Ljava/lang/String;

    invoke-direct {v3, v0, v1}, Laqk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Laqd;->a:Lbsu;

    .line 63
    invoke-virtual {v0, v3}, Lbsu;->a(Ljava/lang/Object;)Lbsv;

    move-result-object v0

    .line 64
    if-nez v0, :cond_16

    const/4 v0, 0x0

    .line 65
    :goto_4
    check-cast v0, Lbml;

    .line 66
    iget-object v1, v2, Lbml;->q:Lbko$a;

    sget-object v5, Lbko$a;->a:Lbko$a;

    if-eq v1, v5, :cond_17

    move v1, v4

    .line 67
    :goto_5
    sget-object v5, Lbml;->a:Lbml;

    .line 68
    invoke-static {v0, v5}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    if-eqz v1, :cond_18

    .line 69
    :cond_9
    invoke-virtual {v2, v0}, Lbml;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_18

    move v0, v4

    .line 70
    :goto_6
    iget-object v1, p0, Laqd;->a:Lbsu;

    invoke-virtual {v1, v3, v2}, Lbsu;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 71
    iget-object v1, p0, Laqd;->h:Lbmm;

    iget-object v3, p1, Laqh;->a:Ljava/lang/String;

    iget-object v5, p1, Laqh;->b:Ljava/lang/String;

    iget-object v7, p1, Laqh;->c:Lbml;

    .line 72
    iget-object v8, v1, Lbmm;->b:Landroid/content/Context;

    const-string v9, "android.permission.WRITE_CALL_LOG"

    invoke-static {v8, v9}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_13

    .line 73
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 75
    if-eqz v7, :cond_19

    .line 76
    iget-object v9, v2, Lbml;->d:Ljava/lang/String;

    iget-object v10, v7, Lbml;->d:Ljava/lang/String;

    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_a

    .line 77
    const-string v6, "name"

    iget-object v9, v2, Lbml;->d:Ljava/lang/String;

    invoke-virtual {v8, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v4

    .line 79
    :cond_a
    iget v9, v2, Lbml;->f:I

    iget v10, v7, Lbml;->f:I

    if-eq v9, v10, :cond_b

    .line 80
    const-string v6, "numbertype"

    iget v9, v2, Lbml;->f:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move v6, v4

    .line 82
    :cond_b
    iget-object v9, v2, Lbml;->g:Ljava/lang/String;

    iget-object v10, v7, Lbml;->g:Ljava/lang/String;

    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_c

    .line 83
    const-string v6, "numberlabel"

    iget-object v9, v2, Lbml;->g:Ljava/lang/String;

    invoke-virtual {v8, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v4

    .line 85
    :cond_c
    iget-object v9, v2, Lbml;->b:Landroid/net/Uri;

    iget-object v10, v7, Lbml;->b:Landroid/net/Uri;

    invoke-static {v9, v10}, Lbib;->a(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v9

    if-nez v9, :cond_d

    .line 86
    const-string v6, "lookup_uri"

    iget-object v9, v2, Lbml;->b:Landroid/net/Uri;

    invoke-static {v9}, Lbib;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v4

    .line 88
    :cond_d
    iget-object v9, v2, Lbml;->k:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_e

    iget-object v9, v2, Lbml;->k:Ljava/lang/String;

    iget-object v10, v7, Lbml;->k:Ljava/lang/String;

    .line 89
    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_e

    .line 90
    const-string v6, "normalized_number"

    iget-object v9, v2, Lbml;->k:Ljava/lang/String;

    invoke-virtual {v8, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v4

    .line 92
    :cond_e
    iget-object v9, v2, Lbml;->h:Ljava/lang/String;

    iget-object v10, v7, Lbml;->h:Ljava/lang/String;

    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_f

    .line 93
    const-string v6, "matched_number"

    iget-object v9, v2, Lbml;->h:Ljava/lang/String;

    invoke-virtual {v8, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v4

    .line 95
    :cond_f
    iget-wide v10, v2, Lbml;->l:J

    iget-wide v12, v7, Lbml;->l:J

    cmp-long v9, v10, v12

    if-eqz v9, :cond_10

    .line 96
    const-string v6, "photo_id"

    iget-wide v10, v2, Lbml;->l:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move v6, v4

    .line 98
    :cond_10
    iget-object v9, v2, Lbml;->m:Landroid/net/Uri;

    invoke-static {v9}, Lbib;->d(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v9

    .line 99
    iget-object v10, v7, Lbml;->m:Landroid/net/Uri;

    invoke-static {v9, v10}, Lbib;->a(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v10

    if-nez v10, :cond_11

    .line 100
    const-string v6, "photo_uri"

    invoke-static {v9}, Lbib;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v4

    .line 102
    :cond_11
    iget-object v9, v2, Lbml;->i:Ljava/lang/String;

    iget-object v10, v7, Lbml;->i:Ljava/lang/String;

    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_12

    .line 103
    const-string v6, "formatted_number"

    iget-object v9, v2, Lbml;->i:Ljava/lang/String;

    invoke-virtual {v8, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v4

    .line 105
    :cond_12
    iget-object v9, v2, Lbml;->j:Ljava/lang/String;

    iget-object v7, v7, Lbml;->j:Ljava/lang/String;

    invoke-static {v9, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1b

    .line 106
    const-string v6, "geocoded_location"

    iget-object v7, v2, Lbml;->j:Ljava/lang/String;

    invoke-virtual {v8, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :goto_7
    if-eqz v4, :cond_13

    .line 123
    if-nez v5, :cond_1a

    .line 124
    :try_start_0
    iget-object v4, v1, Lbmm;->b:Landroid/content/Context;

    .line 125
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iget-object v1, v1, Lbmm;->b:Landroid/content/Context;

    .line 126
    invoke-static {v1}, Lbsp;->e(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    const-string v5, "number = ? AND countryiso IS NULL"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    .line 127
    invoke-virtual {v4, v1, v8, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :cond_13
    :goto_8
    invoke-virtual {p1}, Laqh;->a()Z

    move-result v1

    if-nez v1, :cond_14

    .line 136
    iget-object v1, p0, Laqd;->h:Lbmm;

    .line 137
    iget-object v3, v1, Lbmm;->c:Lbmi;

    if-eqz v3, :cond_14

    .line 138
    invoke-static {v2}, Lbmm;->a(Lbml;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 139
    iget-object v3, v1, Lbmm;->c:Lbmi;

    .line 140
    invoke-interface {v3, v2}, Lbmi;->a(Lbml;)Lbmj;

    move-result-object v2

    .line 141
    iget-object v3, v1, Lbmm;->c:Lbmi;

    iget-object v1, v1, Lbmm;->b:Landroid/content/Context;

    invoke-interface {v3, v1, v2}, Lbmi;->a(Landroid/content/Context;Lbmj;)V

    :cond_14
    move v6, v0

    .line 142
    goto/16 :goto_1

    .line 57
    :cond_15
    invoke-virtual {v1, v2, v3}, Lbmm;->b(Ljava/lang/String;Ljava/lang/String;)Lbml;

    move-result-object v0

    goto/16 :goto_2

    .line 64
    :cond_16
    invoke-virtual {v0}, Lbsv;->a()Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_4

    :cond_17
    move v1, v6

    .line 66
    goto/16 :goto_5

    :cond_18
    move v0, v6

    .line 69
    goto/16 :goto_6

    .line 109
    :cond_19
    const-string v6, "name"

    iget-object v7, v2, Lbml;->d:Ljava/lang/String;

    invoke-virtual {v8, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const-string v6, "numbertype"

    iget v7, v2, Lbml;->f:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v8, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 111
    const-string v6, "numberlabel"

    iget-object v7, v2, Lbml;->g:Ljava/lang/String;

    invoke-virtual {v8, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v6, "lookup_uri"

    iget-object v7, v2, Lbml;->b:Landroid/net/Uri;

    invoke-static {v7}, Lbib;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string v6, "matched_number"

    iget-object v7, v2, Lbml;->h:Ljava/lang/String;

    invoke-virtual {v8, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string v6, "normalized_number"

    iget-object v7, v2, Lbml;->k:Ljava/lang/String;

    invoke-virtual {v8, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v6, "photo_id"

    iget-wide v10, v2, Lbml;->l:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v8, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 116
    const-string v6, "photo_uri"

    iget-object v7, v2, Lbml;->m:Landroid/net/Uri;

    .line 117
    invoke-static {v7}, Lbib;->d(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v7

    invoke-static {v7}, Lbib;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v7

    .line 118
    invoke-virtual {v8, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v6, "formatted_number"

    iget-object v7, v2, Lbml;->i:Ljava/lang/String;

    invoke-virtual {v8, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v6, "geocoded_location"

    iget-object v7, v2, Lbml;->j:Ljava/lang/String;

    invoke-virtual {v8, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 128
    :cond_1a
    :try_start_1
    iget-object v4, v1, Lbmm;->b:Landroid/content/Context;

    .line 129
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iget-object v1, v1, Lbmm;->b:Landroid/content/Context;

    .line 130
    invoke-static {v1}, Lbsp;->e(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    const-string v6, "number = ? AND countryiso = ?"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v3, v7, v9

    const/4 v3, 0x1

    aput-object v5, v7, v3

    .line 131
    invoke-virtual {v4, v1, v8, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_8

    .line 133
    :catch_0
    move-exception v1

    .line 134
    sget-object v3, Lbmm;->a:Ljava/lang/String;

    const-string v4, "Unable to update contact info in call log db"

    invoke-static {v3, v4, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_8

    :cond_1b
    move v4, v6

    goto/16 :goto_7

    :cond_1c
    move-object v2, v1

    goto/16 :goto_3
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 151
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Laqd;->d:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 152
    iget-object v0, p0, Laqd;->f:Laqg;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Laqd;->f:Laqg;

    .line 154
    const/4 v1, 0x1

    iput-boolean v1, v0, Laqg;->a:Z

    .line 155
    iget-object v0, p0, Laqd;->f:Laqg;

    invoke-virtual {v0}, Laqg;->interrupt()V

    .line 156
    const/4 v0, 0x0

    iput-object v0, p0, Laqd;->f:Laqg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 157
    :cond_0
    monitor-exit p0

    return-void

    .line 151
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
