.class public final enum Lfcj;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lfcj;

.field public static final enum b:Lfcj;

.field public static final enum c:Lfcj;

.field public static final enum d:Lfcj;

.field public static final enum e:Lfcj;

.field private static synthetic g:[Lfcj;


# instance fields
.field public final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 6
    new-instance v0, Lfcj;

    const-string v1, "UNKNOWN"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v3, v2}, Lfcj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lfcj;->a:Lfcj;

    .line 7
    new-instance v0, Lfcj;

    const-string v1, "SEND_RECEIVE"

    const-string v2, "sendrecv"

    invoke-direct {v0, v1, v4, v2}, Lfcj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lfcj;->b:Lfcj;

    .line 8
    new-instance v0, Lfcj;

    const-string v1, "SEND_ONLY"

    const-string v2, "sendonly"

    invoke-direct {v0, v1, v5, v2}, Lfcj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lfcj;->c:Lfcj;

    .line 9
    new-instance v0, Lfcj;

    const-string v1, "RECEIVE_ONLY"

    const-string v2, "recvonly"

    invoke-direct {v0, v1, v6, v2}, Lfcj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lfcj;->d:Lfcj;

    .line 10
    new-instance v0, Lfcj;

    const-string v1, "INACTIVE"

    const-string v2, "inactive"

    invoke-direct {v0, v1, v7, v2}, Lfcj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lfcj;->e:Lfcj;

    .line 11
    const/4 v0, 0x5

    new-array v0, v0, [Lfcj;

    sget-object v1, Lfcj;->a:Lfcj;

    aput-object v1, v0, v3

    sget-object v1, Lfcj;->b:Lfcj;

    aput-object v1, v0, v4

    sget-object v1, Lfcj;->c:Lfcj;

    aput-object v1, v0, v5

    sget-object v1, Lfcj;->d:Lfcj;

    aput-object v1, v0, v6

    sget-object v1, Lfcj;->e:Lfcj;

    aput-object v1, v0, v7

    sput-object v0, Lfcj;->g:[Lfcj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput-object p3, p0, Lfcj;->f:Ljava/lang/String;

    .line 4
    new-instance v0, Lfch;

    const/4 v1, 0x0

    invoke-direct {v0, p3, v1}, Lfch;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 5
    return-void
.end method

.method public static values()[Lfcj;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lfcj;->g:[Lfcj;

    invoke-virtual {v0}, [Lfcj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfcj;

    return-object v0
.end method
