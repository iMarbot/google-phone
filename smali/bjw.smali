.class public Lbjw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbjg;
.implements Lcjs;


# instance fields
.field public final a:Lbku;

.field public final b:Landroid/net/ConnectivityManager;

.field public final c:Ldnn;

.field public final d:Lbjf;

.field public final e:Lcjt;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/util/Set;

.field public h:I

.field public i:J

.field public j:J

.field public k:J

.field public l:I

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Z


# direct methods
.method public constructor <init>(Lbku;Landroid/net/ConnectivityManager;Lbjf;Lcjt;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, -0x1

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lpf;

    invoke-direct {v0}, Lpf;-><init>()V

    iput-object v0, p0, Lbjw;->g:Ljava/util/Set;

    .line 20
    iput v1, p0, Lbjw;->h:I

    .line 21
    iput-wide v2, p0, Lbjw;->i:J

    .line 22
    iput-wide v2, p0, Lbjw;->j:J

    .line 23
    iput-wide v2, p0, Lbjw;->k:J

    .line 24
    iput v1, p0, Lbjw;->l:I

    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbjw;->m:Z

    .line 26
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbku;

    iput-object v0, p0, Lbjw;->a:Lbku;

    .line 27
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lbjw;->b:Landroid/net/ConnectivityManager;

    .line 28
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjf;

    iput-object v0, p0, Lbjw;->d:Lbjf;

    .line 29
    invoke-static {p4}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcjt;

    iput-object v0, p0, Lbjw;->e:Lcjt;

    .line 30
    invoke-static {p5}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lbjw;->f:Ljava/lang/String;

    .line 31
    new-instance v0, Ldnn;

    .line 32
    invoke-direct {v0, p4}, Ldnn;-><init>(Lcjt;)V

    .line 33
    iput-object v0, p0, Lbjw;->c:Ldnn;

    .line 34
    invoke-interface {p3, p0}, Lbjf;->a(Lbjg;)V

    .line 35
    invoke-interface {p3, p0}, Lbjf;->a(Lbjw;)V

    .line 36
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcjk;)Lcjl;
    .locals 1

    .prologue
    .line 47
    new-instance v0, Ldnm;

    invoke-direct {v0, p1, p2, p0}, Ldnm;-><init>(Landroid/content/Context;Lcjk;Lbjw;)V

    return-object v0
.end method

.method public a(I)V
    .locals 5

    .prologue
    .line 61
    iget v0, p0, Lbjw;->l:I

    if-eq p1, v0, :cond_0

    .line 62
    const-string v0, "RcsVideoShare.setSessionModificationState"

    const-string v1, "%d -> %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lbjw;->l:I

    .line 63
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 64
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 65
    iput p1, p0, Lbjw;->l:I

    .line 66
    iget-object v0, p0, Lbjw;->e:Lcjt;

    invoke-interface {v0}, Lcjt;->H()V

    .line 67
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v4, 0x0

    .line 1
    invoke-virtual {p0, p1}, Lbjw;->f(Landroid/content/Context;)V

    .line 2
    iget-wide v0, p0, Lbjw;->j:J

    invoke-virtual {p0, v0, v1}, Lbjw;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    const-string v0, "RcsVideoShare.onVideoShareChanged"

    const-string v1, "transmitting session closed"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4
    iput-wide v6, p0, Lbjw;->j:J

    .line 5
    iget v0, p0, Lbjw;->l:I

    invoke-static {v0}, Lbvs;->e(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6
    invoke-virtual {p0, v4}, Lbjw;->a(I)V

    .line 7
    :cond_0
    iget-wide v0, p0, Lbjw;->k:J

    invoke-virtual {p0, v0, v1}, Lbjw;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 8
    const-string v0, "RcsVideoShare.onVideoShareChanged"

    const-string v1, "receiving session closed"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9
    iput-wide v6, p0, Lbjw;->k:J

    .line 10
    :cond_1
    invoke-virtual {p0}, Lbjw;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbjw;->d:Lbjf;

    iget-wide v2, p0, Lbjw;->j:J

    .line 11
    invoke-interface {v0, v2, v3}, Lbjf;->d(J)Lbjx;

    move-result-object v0

    invoke-interface {v0}, Lbjx;->b()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget v0, p0, Lbjw;->l:I

    .line 12
    invoke-static {v0}, Lbvs;->e(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 13
    const-string v0, "RcsVideoShare.onVideoShareChanged"

    const-string v1, "video request invite was accepted by remote party"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    invoke-virtual {p0, v4}, Lbjw;->a(I)V

    .line 15
    invoke-virtual {p0}, Lbjw;->q()V

    .line 16
    :cond_2
    iget-object v0, p0, Lbjw;->e:Lcjt;

    invoke-interface {v0}, Lcjt;->G()V

    .line 17
    return-void
.end method

.method public a(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 48
    iget v0, p0, Lbjw;->h:I

    if-eq v0, p2, :cond_2

    .line 49
    const/16 v0, 0xa

    if-ne p2, v0, :cond_0

    .line 50
    invoke-virtual {p0}, Lbjw;->r()V

    .line 51
    :cond_0
    iget v0, p0, Lbjw;->h:I

    if-eq p2, v0, :cond_1

    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    .line 52
    iget-object v0, p0, Lbjw;->d:Lbjf;

    iget-object v1, p0, Lbjw;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Lbjf;->a(Ljava/lang/String;)V

    .line 53
    :cond_1
    iput p2, p0, Lbjw;->h:I

    .line 54
    :cond_2
    iget-object v0, p0, Lbjw;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnm;

    .line 55
    invoke-virtual {v0}, Ldnm;->h()V

    goto :goto_0

    .line 57
    :cond_3
    return-void
.end method

.method public a(Landroid/view/SurfaceView;Landroid/view/SurfaceView;)V
    .locals 4

    .prologue
    .line 142
    const-string v0, "RcsVideoShare.setSurfaceViews"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lbjw;->d:Lbjf;

    iget-wide v2, p0, Lbjw;->j:J

    .line 144
    invoke-interface {v0, v2, v3}, Lbjf;->d(J)Lbjx;

    move-result-object v0

    .line 145
    iget-object v1, p0, Lbjw;->d:Lbjf;

    iget-wide v2, p0, Lbjw;->k:J

    invoke-interface {v1, v2, v3}, Lbjf;->d(J)Lbjx;

    move-result-object v1

    .line 146
    if-eqz v0, :cond_0

    .line 147
    invoke-virtual {p1}, Landroid/view/SurfaceView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lbvs;->d(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 148
    invoke-interface {v0, p1}, Lbjx;->a(Landroid/view/SurfaceView;)V

    .line 149
    :cond_0
    if-eqz v1, :cond_1

    .line 150
    invoke-interface {v1, p2}, Lbjx;->a(Landroid/view/SurfaceView;)V

    .line 151
    :cond_1
    return-void
.end method

.method public a(Ldnm;)V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lbjw;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 153
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 109
    invoke-virtual {p0}, Lbjw;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 112
    :goto_0
    return-void

    .line 111
    :cond_0
    iget-object v0, p0, Lbjw;->d:Lbjf;

    iget-wide v2, p0, Lbjw;->j:J

    invoke-interface {v0, v2, v3}, Lbjf;->d(J)Lbjx;

    move-result-object v0

    invoke-interface {v0, p1}, Lbjx;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 43
    iget-wide v0, p0, Lbjw;->j:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lbjw;->k:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)Z
    .locals 3

    .prologue
    .line 138
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbjw;->d:Lbjf;

    .line 139
    invoke-interface {v0, p1, p2}, Lbjf;->d(J)Lbjx;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 140
    :goto_0
    return v0

    .line 139
    :cond_0
    const/4 v0, 0x0

    .line 140
    goto :goto_0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 113
    return-void
.end method

.method public b(Ldnm;)V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lbjw;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 155
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    return v0
.end method

.method public b(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 37
    iget v1, p0, Lbjw;->h:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    .line 42
    :cond_0
    :goto_0
    return v0

    .line 39
    :cond_1
    invoke-virtual {p0}, Lbjw;->s()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 41
    iget-object v1, p0, Lbjw;->d:Lbjf;

    iget-object v2, p0, Lbjw;->f:Ljava/lang/String;

    invoke-interface {v1, v2}, Lbjf;->b(Ljava/lang/String;)Lbjb;

    move-result-object v1

    .line 42
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lbjb;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 68
    const-string v0, "RcsVideoShare.upgradeToVideo"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lbjw;->d:Lbjf;

    iget-object v1, p0, Lbjw;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Lbjf;->d(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lbjw;->j:J

    .line 70
    iget-wide v0, p0, Lbjw;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 71
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbjw;->a(I)V

    .line 72
    iget-object v0, p0, Lbjw;->a:Lbku;

    sget-object v1, Lbkq$a;->bL:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 73
    :cond_0
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    return v0
.end method

.method public d(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 74
    const-string v0, "RcsVideoShare.acceptVideoRequest"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lbjw;->d:Lbjf;

    iget-wide v2, p0, Lbjw;->i:J

    invoke-interface {v0, v2, v3}, Lbjf;->c(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-wide v0, p0, Lbjw;->i:J

    iput-wide v0, p0, Lbjw;->k:J

    .line 77
    :cond_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbjw;->i:J

    .line 78
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbjw;->a(I)V

    .line 79
    invoke-virtual {p0}, Lbjw;->q()V

    .line 80
    invoke-virtual {p0}, Lbjw;->j()Z

    move-result v0

    if-nez v0, :cond_1

    .line 81
    invoke-virtual {p0, p1}, Lbjw;->c(Landroid/content/Context;)V

    .line 82
    :cond_1
    iget-object v0, p0, Lbjw;->a:Lbku;

    sget-object v1, Lbkq$a;->bM:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 83
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lbjw;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbjw;->p:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 0

    .prologue
    .line 58
    invoke-virtual {p0}, Lbjw;->r()V

    .line 59
    return-void
.end method

.method public e(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 98
    const-string v0, "RcsVideoShare.resumeTransmission"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 99
    invoke-virtual {p0, p1}, Lbjw;->c(Landroid/content/Context;)V

    .line 100
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 116
    iget-object v0, p0, Lbjw;->e:Lcjt;

    invoke-interface {v0}, Lcjt;->G()V

    .line 117
    iget-object v0, p0, Lbjw;->d:Lbjf;

    iget-object v2, p0, Lbjw;->f:Ljava/lang/String;

    invoke-interface {v0, v2}, Lbjf;->b(Ljava/lang/String;)Lbjb;

    move-result-object v0

    .line 118
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lbjb;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 119
    :goto_0
    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbjw;->n:Z

    if-nez v0, :cond_0

    .line 120
    iget-object v0, p0, Lbjw;->b:Landroid/net/ConnectivityManager;

    iget-object v2, p0, Lbjw;->c:Ldnn;

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->registerDefaultNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 121
    iput-boolean v1, p0, Lbjw;->n:Z

    .line 122
    :cond_0
    return-void

    .line 118
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 124
    iget-wide v0, p0, Lbjw;->i:J

    .line 125
    iget-object v2, p0, Lbjw;->d:Lbjf;

    iget-object v3, p0, Lbjw;->f:Ljava/lang/String;

    invoke-interface {v2, v3}, Lbjf;->e(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lbjw;->i:J

    .line 126
    iget-wide v2, p0, Lbjw;->i:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 127
    iget v0, p0, Lbjw;->l:I

    invoke-static {v0}, Lbvs;->f(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbjw;->a(I)V

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    iget-wide v2, p0, Lbjw;->i:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {p0}, Lbjw;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 133
    invoke-virtual {p0, p1}, Lbjw;->d(Landroid/content/Context;)V

    goto :goto_0

    .line 134
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbjw;->a(I)V

    .line 135
    iget-object v0, p0, Lbjw;->e:Lcjt;

    invoke-interface {v0}, Lcjt;->I()V

    .line 136
    iget-object v0, p0, Lbjw;->a:Lbku;

    sget-object v1, Lbkq$a;->bO:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lbjw;->l:I

    return v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    .line 85
    throw v0
.end method

.method public i()V
    .locals 4

    .prologue
    .line 86
    const-string v0, "RcsVideoShare.declineUpgradeRequest"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lbjw;->d:Lbjf;

    iget-object v1, p0, Lbjw;->d:Lbjf;

    iget-object v2, p0, Lbjw;->f:Ljava/lang/String;

    .line 88
    invoke-interface {v1, v2}, Lbjf;->e(Ljava/lang/String;)J

    move-result-wide v2

    .line 89
    invoke-interface {v0, v2, v3}, Lbjf;->e(J)V

    .line 90
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbjw;->i:J

    .line 91
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbjw;->a(I)V

    .line 92
    iget-object v0, p0, Lbjw;->a:Lbku;

    sget-object v1, Lbkq$a;->bN:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 93
    return-void
.end method

.method public j()Z
    .locals 4

    .prologue
    .line 94
    iget-wide v0, p0, Lbjw;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()V
    .locals 4

    .prologue
    .line 95
    const-string v0, "RcsVideoShare.stopTransmission"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lbjw;->d:Lbjf;

    iget-wide v2, p0, Lbjw;->j:J

    invoke-interface {v0, v2, v3}, Lbjf;->e(J)V

    .line 97
    return-void
.end method

.method public l()V
    .locals 4

    .prologue
    .line 101
    invoke-virtual {p0}, Lbjw;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lbjw;->d:Lbjf;

    iget-wide v2, p0, Lbjw;->j:J

    invoke-interface {v0, v2, v3}, Lbjf;->d(J)Lbjx;

    move-result-object v0

    invoke-interface {v0}, Lbjx;->c()V

    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbjw;->p:Z

    .line 104
    :cond_0
    return-void
.end method

.method public m()V
    .locals 4

    .prologue
    .line 105
    invoke-virtual {p0}, Lbjw;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lbjw;->d:Lbjf;

    iget-wide v2, p0, Lbjw;->j:J

    invoke-interface {v0, v2, v3}, Lbjf;->d(J)Lbjx;

    move-result-object v0

    invoke-interface {v0}, Lbjx;->d()V

    .line 107
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbjw;->p:Z

    .line 108
    :cond_0
    return-void
.end method

.method public n()V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lbjw;->e:Lcjt;

    sget-object v1, Lbkq$a;->cY:Lbkq$a;

    invoke-interface {v0, v1}, Lcjt;->a(Lbkq$a;)V

    .line 115
    return-void
.end method

.method public o()Lblf$a;
    .locals 1

    .prologue
    .line 123
    sget-object v0, Lblf$a;->d:Lblf$a;

    return-object v0
.end method

.method public p()Z
    .locals 4

    .prologue
    .line 141
    iget-wide v0, p0, Lbjw;->k:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lbjw;->e:Lcjt;

    iget-boolean v1, p0, Lbjw;->m:Z

    invoke-interface {v0, v1}, Lcjt;->a(Z)V

    .line 157
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbjw;->m:Z

    .line 158
    return-void
.end method

.method public r()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 159
    iget-boolean v0, p0, Lbjw;->o:Z

    if-eqz v0, :cond_1

    .line 172
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbjw;->o:Z

    .line 162
    iget-boolean v0, p0, Lbjw;->n:Z

    if-eqz v0, :cond_2

    .line 163
    iget-object v0, p0, Lbjw;->b:Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lbjw;->c:Ldnn;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 164
    :cond_2
    iget-object v0, p0, Lbjw;->d:Lbjf;

    invoke-interface {v0, p0}, Lbjf;->b(Lbjw;)V

    .line 165
    iget-object v0, p0, Lbjw;->d:Lbjf;

    invoke-interface {v0, p0}, Lbjf;->b(Lbjg;)V

    .line 166
    iget-wide v0, p0, Lbjw;->j:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 167
    iget-object v0, p0, Lbjw;->d:Lbjf;

    iget-wide v2, p0, Lbjw;->j:J

    invoke-interface {v0, v2, v3}, Lbjf;->e(J)V

    .line 168
    iput-wide v4, p0, Lbjw;->j:J

    .line 169
    :cond_3
    iget-wide v0, p0, Lbjw;->k:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lbjw;->d:Lbjf;

    iget-wide v2, p0, Lbjw;->k:J

    invoke-interface {v0, v2, v3}, Lbjf;->e(J)V

    .line 171
    iput-wide v4, p0, Lbjw;->k:J

    goto :goto_0
.end method

.method public s()Z
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lbjw;->b:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 174
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
