.class final Lbfd;
.super Lbfi;
.source "PG"


# instance fields
.field private a:Lamg;

.field private b:Lbfk;

.field private c:Ljava/lang/CharSequence;

.field private d:Ljava/lang/CharSequence;

.field private e:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lamg;Lbfk;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lbfi;-><init>()V

    .line 2
    iput-object p1, p0, Lbfd;->a:Lamg;

    .line 3
    iput-object p2, p0, Lbfd;->b:Lbfk;

    .line 4
    iput-object p3, p0, Lbfd;->c:Ljava/lang/CharSequence;

    .line 5
    iput-object p4, p0, Lbfd;->d:Ljava/lang/CharSequence;

    .line 6
    iput-object p5, p0, Lbfd;->e:Landroid/content/Intent;

    .line 7
    return-void
.end method


# virtual methods
.method public final a()Lamg;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lbfd;->a:Lamg;

    return-object v0
.end method

.method public final b()Lbfk;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lbfd;->b:Lbfk;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lbfd;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lbfd;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final e()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lbfd;->e:Landroid/content/Intent;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 14
    if-ne p1, p0, :cond_1

    .line 24
    :cond_0
    :goto_0
    return v0

    .line 16
    :cond_1
    instance-of v2, p1, Lbfi;

    if-eqz v2, :cond_7

    .line 17
    check-cast p1, Lbfi;

    .line 18
    iget-object v2, p0, Lbfd;->a:Lamg;

    if-nez v2, :cond_3

    invoke-virtual {p1}, Lbfi;->a()Lamg;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    iget-object v2, p0, Lbfd;->b:Lbfk;

    .line 19
    invoke-virtual {p1}, Lbfi;->b()Lbfk;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lbfd;->c:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 20
    invoke-virtual {p1}, Lbfi;->c()Ljava/lang/CharSequence;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_2
    iget-object v2, p0, Lbfd;->d:Ljava/lang/CharSequence;

    if-nez v2, :cond_5

    .line 21
    invoke-virtual {p1}, Lbfi;->d()Ljava/lang/CharSequence;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_3
    iget-object v2, p0, Lbfd;->e:Landroid/content/Intent;

    if-nez v2, :cond_6

    .line 22
    invoke-virtual {p1}, Lbfi;->e()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    .line 23
    goto :goto_0

    .line 18
    :cond_3
    iget-object v2, p0, Lbfd;->a:Lamg;

    invoke-virtual {p1}, Lbfi;->a()Lamg;

    move-result-object v3

    invoke-virtual {v2, v3}, Lamg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    .line 20
    :cond_4
    iget-object v2, p0, Lbfd;->c:Ljava/lang/CharSequence;

    invoke-virtual {p1}, Lbfi;->c()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_2

    .line 21
    :cond_5
    iget-object v2, p0, Lbfd;->d:Ljava/lang/CharSequence;

    invoke-virtual {p1}, Lbfi;->d()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_3

    .line 22
    :cond_6
    iget-object v2, p0, Lbfd;->e:Landroid/content/Intent;

    invoke-virtual {p1}, Lbfi;->e()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_7
    move v0, v1

    .line 24
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const v3, 0xf4243

    .line 25
    iget-object v0, p0, Lbfd;->a:Lamg;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    xor-int/2addr v0, v3

    .line 26
    mul-int/2addr v0, v3

    .line 27
    iget-object v2, p0, Lbfd;->b:Lbfk;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    .line 28
    mul-int v2, v0, v3

    .line 29
    iget-object v0, p0, Lbfd;->c:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    xor-int/2addr v0, v2

    .line 30
    mul-int v2, v0, v3

    .line 31
    iget-object v0, p0, Lbfd;->d:Ljava/lang/CharSequence;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    xor-int/2addr v0, v2

    .line 32
    mul-int/2addr v0, v3

    .line 33
    iget-object v2, p0, Lbfd;->e:Landroid/content/Intent;

    if-nez v2, :cond_3

    :goto_3
    xor-int/2addr v0, v1

    .line 34
    return v0

    .line 25
    :cond_0
    iget-object v0, p0, Lbfd;->a:Lamg;

    invoke-virtual {v0}, Lamg;->hashCode()I

    move-result v0

    goto :goto_0

    .line 29
    :cond_1
    iget-object v0, p0, Lbfd;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1

    .line 31
    :cond_2
    iget-object v0, p0, Lbfd;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_2

    .line 33
    :cond_3
    iget-object v1, p0, Lbfd;->e:Landroid/content/Intent;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 13
    iget-object v0, p0, Lbfd;->a:Lamg;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbfd;->b:Lbfk;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbfd;->c:Ljava/lang/CharSequence;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lbfd;->d:Ljava/lang/CharSequence;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lbfd;->e:Landroid/content/Intent;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x54

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "ContactPrimaryActionInfo{number="

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", photoInfo="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", primaryText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", secondaryText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", intent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
